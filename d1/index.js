console.log("Hello World");

/*function getDetails(username){
	
	if(username == null || "") {
		alert("Input must not be empty");
	}else{
		switch (username) {
			case 1:
			username = 'natsu'
			break;

			case 2:
			username = 'naruto'
			break;

			case 3:
			username = 'sasuke'
			break;

			case 4:
			username = 'gildart'
			break;

			case 5:
			username = 'jiraiya'
			break;

			default:
			username = getDetails + 'not registered'
			break;
		}
	}
	return username;
}
console.log(getDetails(5))*/


//While Loop
/*
	-takes in an expression/condition
	-expressions are any unit of code that can be evaluated to a value
	-if the evaluation of the condition is true, the statements will be executed.

	Syntax:
	while(expression/condition){
	statement/s;
	}

	*/
/*let count = 5;
while (count !== 0) {
	console.log("While Loop result " +count);
	count--;
}


count = 0;
while (count <= 10) {
	console.log("While Loop result " +count);
	count++;
}*/

//Do-While Loop
	/*Syntax:
		do{
			statement/s
		}while(expression/condition)*/
/*
let number = Number(prompt("Give me a number: "));
do{
	console.log("Do-While Loop: " + number);
	number+=1;
}while(number<10)*/

//For Loop
	/*
	1. initialization
	2.condition/expression
	3.finalExpression
	*/

/*	for(let count = 0; count <=20; count++){
		console.log("For Loop: " +count);
	}*/

//using strings

let myString = "alex";
console.log(myString);

for(var x=0; x<= myString.length; x++){
	console.log(myString[x]);
}

let myName = 'NATSU';
console.log(myName);
myName = myName.toLowerCase()

for(var y=0; y<= myName.length; y++){
	if(
		myName[y] === 'a'||
		myName[y] === 'e'||
		myName[y] === 'i'||
		myName[y] === 'o'||
		myName[y] === 'u')
	{
		console.log(3);
	}else{
		console.log(myName[y]);
	}
}


//Continue and Break Statements
/*
	the "continue" allows the code to go to the next iteration of the loop without finishing the execution of all statements in the block

	the "break" is used to terminate the current loop once a match has been found/the condition returns true
*/

/*for(let count = 0; count<=20; count++){
	if(count%2 === 0){
		//tells the code to continue to the next iteration of the loop; it will ignore all preceding of the block
		continue;
	}
	//of the remainder is not equal to zero, this will be executed
	console.log("Continue and Break: " + count);
	if(count>10){
	//tells the code to terminate/stop loop even if the expression/condition of loop will return true for the next iteration.
		break;
	};
};*/


let name = "alexandro";

for(let i = 0; i< name.length; i++){
	console.log(name[i]);
	if(name[i].toLowerCase() === "a"){
		continue;
	}
	if(name[i].toLowerCase() === "d"){
		break;
	}
}




for(let x = 0; x <=3; x++){
	for(let y = 0; y<=x; y++)
		console.log("x: " + x + " y: " + y);
}